//================================================================================
//
// Auter : KENSUKE WATANABE
// Data  : 2018/01/02
//
//================================================================================
#include "xa2Core.h"
#include "xa2MasteringVoice.h"
#include "xa2Debug.h"

//--------------------------------------------------------------------------------
// 初期化処理
//--------------------------------------------------------------------------------
HRESULT XA2MasteringVoice::Init(HWND hWnd)
{
	HRESULT hr;

	// マスターボイスの生成
	UINT32 InputChannels = XAUDIO2_DEFAULT_CHANNELS;
	UINT32 InputSampleRate = XAUDIO2_DEFAULT_SAMPLERATE;
	UINT32 Flags = 0;
	const XAUDIO2_EFFECT_CHAIN *pEffectChain = nullptr;
#if (_WIN32_WINNT <= _WIN32_WINNT_WIN7 )
	UINT32 DeviceIndex = 0;
	hr = XA2Core::GetXAudio2()->CreateMasteringVoice(
		&m_pMasteringVoice,
		InputChannels,
		InputSampleRate,
		Flags,
		DeviceIndex,
		pEffectChain);
#else
	LPCWSTR szDeviceId = nullptr;
	hr = XA2Core::GetXAudio2()->CreateMasteringVoice(
		&m_pMasteringVoice,
		InputChannels,
		InputSampleRate,
		Flags,
		szDeviceId,
		pEffectChain);
#endif

	if(FAILED(hr))
	{
		XA2Manager::SetXAudio2Err("FAILED:CreateMasteringVoice() ");
		XA2Debug::DebugMessageBox(hWnd, "マスターボイスの生成に失敗！", "警告", MB_ICONWARNING);

		return hr;
	}

	return hr;
}

//--------------------------------------------------------------------------------
// 終了処理
//--------------------------------------------------------------------------------
void XA2MasteringVoice::Uninit()
{
	// マスターボイスの破棄
	if (m_pMasteringVoice)
	{
		m_pMasteringVoice->DestroyVoice();
		m_pMasteringVoice = nullptr;
	}
}
