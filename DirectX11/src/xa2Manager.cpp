//================================================================================
//
// Auter : KENSUKE WATANABE
// Data  : 2018/01/02
//
//================================================================================
#include "xa2Manager.h"
#include "xa2Core.h"
#include "xa2MasteringVoice.h"
#include "xa2SourceVoiceManager.h"
#include "xa2SoundResourceManager.h"
#include "xa2ListenerManager.h"

// 静的メンバの初期化
//--------------------------------------------------------------------------------
XA2Core *XA2Manager::m_pXA2Core = nullptr;								// XAudio2のエンジン
XA2MasteringVoice *XA2Manager::m_pMaster = nullptr;						// マスタリングボイス
XA2SourceVoiceManager *XA2Manager::m_pSourceVoiceManager = nullptr;		// サウンドオブジェクトマネージャ
XA2SoundResourceManager *XA2Manager::m_pSoundResourceManager = nullptr;	// サウンドリソースマネージャ
XA2ListenerManager *XA2Manager::m_pListenerManager = nullptr;			// リスナーマネージャ
XA2LoadWaveOnAll *XA2Manager::m_pLoadWaveOnAll;							// wave読み込み - CPU全のせ
XA2LoadWaveStreaming *XA2Manager::m_pLoadWaveStreaming;					// wave読み込み - ストリーミング
XA2LoadOggOnAll *XA2Manager::m_pLoadOggOnAll;							// ogg読み込み - CPU全のせ
XA2LoadOggStreaming *XA2Manager::m_pLoadOggStreaming;					// ogg読み込み - CPUストリーミング

bool XA2Manager::m_xAudio2Err = false;									// XAudio2エラーチェック
std::string XA2Manager::m_errMessage = "";								// 
std::recursive_mutex XA2Manager::m_mutex;								// ミューテクス
X3DAUDIO_HANDLE XA2Manager::m_x3dInstance = { 0 };						// ハンドル

#if (_WIN32_WINNT >= 0x0602 /*_WIN32_WINNT_WIN8*/)
XAUDIO2_VOICE_DETAILS *XA2Manager::m_pDetails;
#else
XAUDIO2_DEVICE_DETAILS *XA2Manager::m_pDetails;
#endif
DWORD XA2Manager::m_channelMask = 0;
UINT32 XA2Manager::m_channels = 0;
UINT32 XA2Manager::m_sampleRate = 0;

//--------------------------------------------------------------------------------
// コンストラクタ
//--------------------------------------------------------------------------------
XA2Manager::XA2Manager(HWND hWnd)
{
	m_xAudio2Err = false;
	m_errMessage = "";

	// XAudio2の初期化
	//--------------------------------------------------------------------------------
	m_pXA2Core = new XA2Core;
	if (FAILED(m_pXA2Core->Init(hWnd)))
	{
		XA2Manager::SetXAudio2Err("FAILED:m_pXA2Core->Init() ");
		// 解放
		if (m_pXA2Core)
		{
			delete m_pXA2Core;
			m_pXA2Core = nullptr;
		}
	}

	// マスターボイス
	//--------------------------------------------------------------------------------
	m_pMaster = new XA2MasteringVoice;
	if (FAILED(m_pMaster->Init(hWnd)))
	{
		XA2Manager::SetXAudio2Err("FAILED:m_pMaster->Init() ");
		// 解放
		if (m_pMaster)
		{
			delete m_pMaster;
			m_pMaster = nullptr;
		}
		// XAudio2の解放
		if (m_pXA2Core)
		{
			m_pXA2Core->Uninit();
			delete m_pXA2Core;
			m_pXA2Core = nullptr;
		}
	}

#if (_WIN32_WINNT >= 0x0602 /*_WIN32_WINNT_WIN8*/)

	m_pDetails = new XAUDIO2_VOICE_DETAILS;
	m_pMaster->GetMasteringVoice()->GetVoiceDetails(m_pDetails);

	if (m_pDetails->InputChannels > OUTPUTCHANNELS)
	{
		XA2Manager::SetXAudio2Err("FAILED:m_pDetails->InputChannels > OUTPUTCHANNELS ");
		ReleaseAll();
		return;
	}

	if (FAILED(m_pMaster->GetMasteringVoice()->GetChannelMask(&m_channelMask)))
	{
		XA2Manager::SetXAudio2Err("FAILED:m_pMaster->GetMasteringVoice()->GetChannelMask() ");
		ReleaseAll();
		return;
	}

	m_sampleRate = m_pDetails->InputSampleRate;
	m_channels = m_pDetails->InputChannels;

#else

	XAUDIO2_DEVICE_DETAILS m_pDetails;
	if (FAILED(m_pMaster->GetMasteringVoice()->GetVoiceDetails(0, m_pDetails)))
	{
		XA2Manager::SetXAudio2Err("FAILED:m_pMaster->GetMasteringVoice()->GetVoiceDetails() ");
		ReleaseAll();
		return;
	}

	if (m_pDetails->OutputFormat.Format.nChannels > OUTPUTCHANNELS)
	{
		XA2Manager::SetXAudio2Err("FAILED:m_pDetails->OutputFormat.Format.nChannels > OUTPUTCHANNELS ");
		ReleaseAll();
		return;
	}

	m_sampleRate = m_pDetails->OutputFormat.Format.nSamplesPerSec;
	m_channelMask = m_pDetails->OutputFormat.dwChannelMask;
	m_channels = m_pDetails->OutputFormat.Format.nChannels;

#endif

	// フォーマット毎の読み込み処理
	//--------------------------------------------------------------------------------
	m_pLoadWaveOnAll = new XA2LoadWaveOnAll();			// wave - CPU全のせ
	m_pLoadWaveStreaming = new XA2LoadWaveStreaming();	// wave - ストリーミング
	m_pLoadOggOnAll = new XA2LoadOggOnAll();			// ogg - CPU全のせ
	m_pLoadOggStreaming = new XA2LoadOggStreaming();	// ogg - ストリーミング

	// ソースボイスマネージャ
	//--------------------------------------------------------------------------------
	m_pSourceVoiceManager = new XA2SourceVoiceManager();

	// サウンドリソースマネージャ
	//--------------------------------------------------------------------------------
	m_pSoundResourceManager = new XA2SoundResourceManager();

	// リスナーマネージャ
	//--------------------------------------------------------------------------------
	m_pListenerManager = new XA2ListenerManager();

	// 3D環境の初期化
	//--------------------------------------------------------------------------------
	if (FAILED(X3DAudioInitialize(m_channelMask, X3DAUDIO_SPEED_OF_SOUND, m_x3dInstance)))
	{
		SetXAudio2Err("FAILED:X3DAudioInitialize() ");
	}
}

//--------------------------------------------------------------------------------
// デストラクタ
//--------------------------------------------------------------------------------
XA2Manager::~XA2Manager()
{
	ReleaseAll();
}

//--------------------------------------------------------------------------------
// クラス内インスタンスの解放
//--------------------------------------------------------------------------------
void XA2Manager::ReleaseAll()
{
	// リスナーマネージャ
	//--------------------------------------------------------------------------------
	if (m_pListenerManager)
	{
		m_pListenerManager->Uninit();
		delete m_pListenerManager;
		m_pListenerManager = nullptr;
	}

	// サウンドオブジェクトマネージャ
	//--------------------------------------------------------------------------------
	if (m_pSourceVoiceManager)
	{
		m_pSourceVoiceManager->Uninit();
		delete m_pSourceVoiceManager;
		m_pSourceVoiceManager = nullptr;
	}

	// サウンドリソースマネージャ
	//--------------------------------------------------------------------------------
	if (m_pSoundResourceManager)
	{
		m_pSoundResourceManager->Uninit();
		delete m_pSoundResourceManager;
		m_pSoundResourceManager = nullptr;
	}

	// wave読み込み - CPU全のせ
	//--------------------------------------------------------------------------------
	if (m_pLoadWaveOnAll)
	{
		delete m_pLoadWaveOnAll;
		m_pLoadWaveOnAll = nullptr;
	}

	// wave読み込み - ストリーミング
	//--------------------------------------------------------------------------------
	if (m_pLoadWaveStreaming)
	{
		delete m_pLoadWaveStreaming;
		m_pLoadWaveStreaming = nullptr;
	}

	// ogg読み込み - CPU全のせ
	//--------------------------------------------------------------------------------
	if (m_pLoadOggOnAll)
	{
		delete m_pLoadOggOnAll;
		m_pLoadOggOnAll = nullptr;
	}

	// ogg読み込み - ストリーミング
	//--------------------------------------------------------------------------------
	if (m_pLoadOggStreaming)
	{
		delete m_pLoadOggStreaming;
		m_pLoadOggStreaming = nullptr;
	}

	// デタイルズ
	//--------------------------------------------------------------------------------
	if (m_pDetails)
	{
		delete m_pDetails;
		m_pDetails = nullptr;
	}

	// マスタリングボイス
	//--------------------------------------------------------------------------------
	if (m_pMaster)
	{
		m_pMaster->Uninit();
		delete m_pMaster;
		m_pMaster = nullptr;
	}

	// XAudio2エンジン
	//--------------------------------------------------------------------------------
	if (m_pXA2Core)
	{
		m_pXA2Core->Uninit();
		delete m_pXA2Core;
		m_pXA2Core = nullptr;
	}
}