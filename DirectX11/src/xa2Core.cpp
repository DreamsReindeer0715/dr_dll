//================================================================================
//
// Auter : KENSUKE WATANABE
// Data  : 2018/01/02
//
//================================================================================
#include "xa2Core.h"
#include "xa2Debug.h"

// 静的メンバ変数
//--------------------------------------------------------------------------------
IXAudio2 *XA2Core::m_pXAudio2 = nullptr;				// XAudio2オブジェクトへのインターフェイス

//--------------------------------------------------------------------------------
// 初期化処理
//--------------------------------------------------------------------------------
HRESULT XA2Core::Init(HWND hWnd)
{
	// COMライブラリの初期化
	CoInitializeEx(nullptr, COINIT_MULTITHREADED);

	UINT32 flags = 0;
#if (_WIN32_WINNT < 0x0602 /*_WIN32_WINNT_WIN8*/) && defined(_DEBUG)
	flags |= XAUDIO2_DEBUG_ENGINE; // デバッグフラグ立てる
#endif

	// XAudio2オブジェクトの作成
	HRESULT hr = XAudio2Create(&m_pXAudio2, flags);
	if(FAILED(hr))
	{
		XA2Manager::SetXAudio2Err("FAILED:XAudio2Create() ");
		XA2Debug::DebugMessageBox(hWnd, "XAudio2オブジェクトの作成に失敗！", "警告", MB_ICONWARNING);
		// COMライブラリの終了処理
		CoUninitialize();
		return hr;
	}

#if (_WIN32_WINNT >= 0x0602 /*_WIN32_WINNT_WIN8*/) && defined(_DEBUG)
	// To see the trace output, you need to view ETW logs for this application:
	//    Go to Control Panel, Administrative Tools, Event Viewer.
	//    View->Show Analytic and Debug Logs.
	//    Applications and Services Logs / Microsoft / Windows / XAudio2. 
	//    Right click on Microsoft Windows XAudio2 debug logging, Properties, then Enable Logging, and hit OK 
	XAUDIO2_DEBUG_CONFIGURATION debug = { 0 };
	debug.TraceMask = XAUDIO2_LOG_ERRORS | XAUDIO2_LOG_WARNINGS;
	debug.BreakMask = XAUDIO2_LOG_ERRORS;
	m_pXAudio2->SetDebugConfiguration(&debug, 0);
#endif

	return S_OK;
}

//--------------------------------------------------------------------------------
// 終了処理
//--------------------------------------------------------------------------------
void XA2Core::Uninit()
{
	if(m_pXAudio2)
	{
		// XAudio2オブジェクトの開放
		m_pXAudio2->Release();
		m_pXAudio2 = nullptr;
	}
	
	// COMライブラリの終了処理
	CoUninitialize();
}
