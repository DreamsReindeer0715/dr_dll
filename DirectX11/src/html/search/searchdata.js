var indexSectionsWithContent =
{
  0: "_acdfgilmnoprstuwx~",
  1: "wx",
  2: "x",
  3: "wx",
  4: "acdfgilmnprsuwx~",
  5: "cdfmw",
  6: "al",
  7: "al",
  8: "_imoprt"
};

var indexSectionNames =
{
  0: "all",
  1: "classes",
  2: "namespaces",
  3: "files",
  4: "functions",
  5: "variables",
  6: "enums",
  7: "enumvalues",
  8: "defines"
};

var indexSectionLabels =
{
  0: "全て",
  1: "クラス",
  2: "名前空間",
  3: "ファイル",
  4: "関数",
  5: "変数",
  6: "列挙型",
  7: "列挙値",
  8: "マクロ定義"
};

