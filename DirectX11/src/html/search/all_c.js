var searchData=
[
  ['readchunkdata',['ReadChunkData',['../class_x_a2_load_audio.html#a47f7155a2f5682cabe7f1f09a3e2723b',1,'XA2LoadAudio']]],
  ['release',['Release',['../class_x_a2_load_ogg_streaming.html#a1204365b04cd50008dfd6551cbb6e08a',1,'XA2LoadOggStreaming::Release()'],['../class_x_a2_load_wave_streaming.html#aa456a722e179368da03e1c08cd7b6cf5',1,'XA2LoadWaveStreaming::Release()']]],
  ['releaseendse',['ReleaseEndSE',['../class_x_a2_source_voice_interface.html#adcd5048fa9c7317ef0f6c7f25fd6a75b',1,'XA2SourceVoiceInterface']]],
  ['return_5fclamp',['RETURN_CLAMP',['../w_math_8h.html#a09dea8476007e4c6b1f19f22918cef3c',1,'wMath.h']]],
  ['return_5fmax',['RETURN_MAX',['../w_math_8h.html#a8e7bb2e00ea5d9af55297fab02af3938',1,'wMath.h']]],
  ['return_5fmin',['RETURN_MIN',['../w_math_8h.html#a4a7bc363eb372e9b8cc13c7df513db29',1,'wMath.h']]],
  ['roty',['RotY',['../namespace_x_m_1_1_f3.html#a1b2c9ce8ac1a5af70ceaca480913df70',1,'XM::F3']]]
];
