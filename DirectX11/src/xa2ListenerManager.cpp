//================================================================================
//
// Auter : KENSUKE WATANABE
//
//================================================================================
#include "xa2Manager.h"
#include "xa2Core.h"
#include "xa2ListenerManager.h"
#include "xa2Listener.h"
#include "xa2Debug.h"

// 静的メンバ
//--------------------------------------------------------------------------------
std::list<XA2Listener*> XA2ListenerManager::m_pListeners;

// 終了処理
//--------------------------------------------------------------------------------
void XA2ListenerManager::Uninit()
{
	for (auto it : m_pListeners)
	{
		if (it)
		{
			delete it;
			it = nullptr;
		}
	}
	m_pListeners.clear();
}

// ID指定取得
//--------------------------------------------------------------------------------
XA2Listener* XA2ListenerManager::GetListener(void *id)
{
	for (auto it : m_pListeners)
	{
		if (it->GetID() == id)
		{
			return it;
		}
	}

	XA2Debug::DebugMessageBox(nullptr, "リスナー取得を失敗しました。", "警告", MB_OK);
	return nullptr;
}

// 追加処理
//--------------------------------------------------------------------------------
void XA2ListenerManager::AddListener(XA2Listener* pListener, void* thisPointerID)
{
	// エラーチェック
	if (XA2Manager::CheckXAudio2Err())
		return;

	if (m_pListeners.size() >= MAX_LISTENER)
	{
		XA2Debug::DebugMessageBox(nullptr, "リスナー生成の上限値です。", "警告", MB_OK);
		return;
	}

	pListener->SetID(thisPointerID);
	m_pListeners.push_back(pListener);
}