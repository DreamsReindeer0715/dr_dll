#pragma once
//================================================================================
//
// Auter : KENSUKE WATANABE
// Data  : 2019/03/21
//
//================================================================================
#ifndef XA2DEBUG_H_
#define XA2DEBUG_H_

#include <Windows.h>

class XA2Debug
{
public:
	static void DebugMessageBox(HWND hWnd, LPCSTR lpText, LPCSTR lpCaption, UINT uType)
	{
#ifdef _DEBUG
		MessageBox(hWnd, lpText, lpCaption, uType);
#endif
	}
};

#endif
