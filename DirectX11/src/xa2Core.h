//================================================================================
//
// Auter : KENSUKE WATANABE
// Data  : 2018/01/02
//
//================================================================================
#ifndef _XA2CORE_H_
#define _XA2CORE_H_

#include <xaudio2.h>
#include "xa2Manager.h"

#define INPUTCHANNELS (1)			// ソースチャンネル番号
#define OUTPUTCHANNELS (8)			// サポートする最大チャンネル(予定)

// XAudio2エンジン
//--------------------------------------------------------------------------------
class XA2Core
{
public:
	XA2Core() {}
	~XA2Core() {}

	static HRESULT Init(HWND hWnd);
	static void Uninit();

	// エンジンの取得
	static IXAudio2 *GetXAudio2() { return m_pXAudio2; }

private:
	// XAudio2オブジェクトへのインターフェイス
	static IXAudio2 *m_pXAudio2;

};

#endif
